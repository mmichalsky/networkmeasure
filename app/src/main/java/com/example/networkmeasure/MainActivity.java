package com.example.networkmeasure;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


        TextView networkCountryIsoValue = findViewById(R.id.networkCountryIsoValue);
        TextView networkTypeValue = findViewById(R.id.networkTypeValue);
        TextView networkOperatorValue = findViewById(R.id.networkOperatorValue);
        TextView registrationState = findViewById(R.id.networkRegistrationStateValue);
        TextView signalStrength = findViewById(R.id.signalStrengthValue);
        TextView cellIdentityValue = findViewById(R.id.cellIdentityValue);
        TextView cellLocValue = findViewById(R.id.cellLocValue);

        String networkCountryIso = tm.getNetworkCountryIso();

        networkCountryIsoValue.setText(networkCountryIso);
        String strNetworkType = "";
        int networkType = tm.getNetworkType();


        switch (networkType) {
            case (TelephonyManager.NETWORK_TYPE_GPRS):
                strNetworkType = "GPRS";
                break;
            case (TelephonyManager.NETWORK_TYPE_EDGE):
                strNetworkType = "EDGE";
                break;
            case (TelephonyManager.NETWORK_TYPE_HSDPA):
                strNetworkType = "HSDPA";
                break;
            case (TelephonyManager.NETWORK_TYPE_LTE):
                strNetworkType = "LTE";
                break;

        }

        networkTypeValue.setText(strNetworkType);

        String networkOperator = tm.getNetworkOperatorName();

        networkOperatorValue.setText(networkOperator);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        List<CellInfo> cellInfos = tm.getAllCellInfo();

        String cellIdentityStr = "";
        String cellSignalStrengthStr = "";
        String registrationStateStr = "";
        for(CellInfo cellInfo : cellInfos)
        {
            CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;

            cellIdentityStr = cellInfoGsm.getCellIdentity().toString();
            cellSignalStrengthStr = cellInfoGsm.getCellSignalStrength().toString();
            registrationStateStr = String.valueOf(cellInfoGsm.isRegistered());


        }



        CellLocation cellLoc = tm.getCellLocation();

        cellLocValue.setText(cellLoc.toString());


            registrationState.setText(registrationStateStr);
            signalStrength.setText(cellSignalStrengthStr);
            cellIdentityValue.setText(cellIdentityStr);

        }

//    ConnectivityManager cm = (ConnectivityManager)this.getSystemService(CONNECTIVITY_SERVICE);
//    NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
//    var downSpeed = nc.getLinkDownstreamBandwidthKbps();
//    var upSpeed = nc.getLinkUpstreamBandwidthKbps();
    }



